package com.kyanja.limitsservice.controller;

import com.kyanja.limitsservice.config.LimitConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitsConfigurationController
{

    @Autowired
    private LimitConfiguration configuration;


    @GetMapping("/limits")
    public LimitConfiguration retriveLimitsConfigurations()
    {
        return new LimitConfiguration(1000, 1);
    }

    @GetMapping("/limits-from-properties")
    public LimitConfiguration retriveLimitsFromConfigurations()
    {
//getting values from the properties file
        return new LimitConfiguration(configuration.getMaximum(), configuration.getMinimum());
    }
}
